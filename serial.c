#include <unistd.h>
#include <strings.h>
#include <errno.h>

#include "serial.h"

// 打开串口
// pdev: 串口名称或设备路径
// baudrate: 波特率
// parity: 奇偶校验
// databits: 数据字节宽度
// stopbits: 停止位
/*******************************************************************************
* Function Name  : serial_set_opt
* Description    : serial setting
* Input          : int fd // file handler
                   int baudrate // 波特率
                   int parity // 校验
                   int databits // 数据位
                   int stopbits // 停止位
* Output         : void
* Return         : int // success:0 fail:-1
*******************************************************************************/
static int serial_set_opt(int fd, int baudrate, int parity, int databits, int stopbits, int vmin, int vtime)
{
    struct termios newtio, oldtio;

    if(tcgetattr(fd, &oldtio) != 0){
        return -1;
    }

    bzero(&newtio, sizeof(newtio));
    newtio.c_cflag |= (CLOCAL | CREAD);
    newtio.c_cflag &= ~CSIZE;
    //设置位数
    switch(databits)
    {
        case 7:
            newtio.c_cflag |= CS7;
            break;
        case 8:
            newtio.c_cflag |= CS8;
            break;
        default:
            newtio.c_cflag |= CS8;
            break;
    }
    //设置奇偶校验位
    switch(parity)
    {
        case 'n':
        case 'N': // 无校验
            newtio.c_cflag &= ~PARENB;
			newtio.c_iflag &= ~INPCK;
            break;
        case 'o':
        case 'O': // 奇校验
            newtio.c_cflag |= PARENB;
            newtio.c_cflag |= PARODD;
            newtio.c_iflag |= INPCK;
            break;
        case 'e':
        case 'E': // 偶校验
            newtio.c_cflag |= PARENB;
            newtio.c_cflag &= ~PARODD;
            newtio.c_iflag |= INPCK;
            break;
        default:
            newtio.c_cflag &= ~PARENB;
			newtio.c_iflag &= ~INPCK;
            break;
    }
    //设置波特率
    switch(baudrate)
    {
        case 4800:
            cfsetispeed(&newtio, B4800);
            cfsetospeed(&newtio, B4800);
            break;
        case 9600:
            cfsetispeed(&newtio, B9600);
            cfsetospeed(&newtio, B9600);
            break;
        case 19200:
            cfsetispeed(&newtio, B19200);
            cfsetospeed(&newtio, B19200);
            break;
        case 38400:
            cfsetispeed(&newtio, B38400);
            cfsetospeed(&newtio, B38400);
            break;
        case 57600:
            cfsetispeed(&newtio, B57600);
            cfsetospeed(&newtio, B57600);
            break;
        case 115200:
            cfsetispeed(&newtio, B115200);
            cfsetospeed(&newtio, B115200);
            break;
        case 460800:
            cfsetispeed(&newtio, B460800);
            cfsetospeed(&newtio, B460800);
            break;
        default:
            cfsetispeed(&newtio, B115200);
            cfsetospeed(&newtio, B115200);
            break;
    }

    //设置停止位
    switch(stopbits)
    {
        case 1:
            newtio.c_cflag &= ~CSTOPB;
            break;
        case 2:
            newtio.c_cflag |= CSTOPB;
            break;
        default:
            newtio.c_cflag &= ~CSTOPB;
            break;
    }

    newtio.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    newtio.c_iflag &= ~(IXON | IXOFF | IXANY);
    newtio.c_iflag &= ~(INLCR | ICRNL | IGNCR);
    newtio.c_oflag &= ~OPOST;
    newtio.c_oflag &= ~(ONLCR | OCRNL);

    tcflush(fd, TCIOFLUSH);
    newtio.c_cc[VTIME] = vtime;
    newtio.c_cc[VMIN] = vmin;
    if((tcsetattr(fd, TCSANOW, &newtio)) != 0)
    {
        return -1;
    }

    return 0;
}

/*******************************************************************************
* Function Name  : serial_open
* Description    : open the serial file
* Input          : const char* pdev // serial file name
                   int baudrate // 波特率
                   int parity // 校验
                   int databits // 数据位
                   int stopbits // 停止位
* Output         : void
* Return         : int // success:>0 fail:-1
*******************************************************************************/
int serial_open(const char* pdev, int baudrate, int parity, int databits, int stopbits, int vmin, int vtime)
{
    int fd;

    fd = open(pdev, O_RDWR | O_NOCTTY | O_NDELAY);
    // check fd valid
    if(fd == -1)
    {
        return -1;
    }
    if(serial_set_opt(fd, baudrate, parity, databits, stopbits, vmin, vtime) == -1)
    {
        return -1;
    }

    return fd;
}

/*******************************************************************************
* Function Name  : serial_close
* Description    : close the serial file
* Input          : int fd // file handler
* Output         : void
* Return         : int // success:0 fail:-1
*******************************************************************************/
int serial_close(int fd)
{
    return close(fd);
}

/*******************************************************************************
* Function Name  : serial_safe_write
* Description    : safe write to file
* Input          : int fd // serial file handler
                   const void *pbuf // data
                   int count // total data len
* Output         : void
* Return         : int // send data len
*******************************************************************************/
static int serial_safe_write(int fd, const void *pbuf, int count)
{
    int ret;

    do {
        ret = write(fd, pbuf, count);
    } while (ret < 0 && (errno == EINTR || errno == EAGAIN));

    return ret;
}

/*******************************************************************************
* Function Name  : serial_write
* Description    : full write to file
* Input          : int fd // serial file handler
                   const void *pbuf // data
                   int count // total data len
* Output         : void
* Return         : int // send data len
*******************************************************************************/
int serial_write(int fd, const void *pbuf, int len)
{
    int w_len, total = 0; 

    while (len) {
        w_len = serial_safe_write(fd, pbuf, len);

        if (w_len < 0) {
            if (total) {
                /* we already wrote some! */
                /* user can do another write to know the error code */
                return total;
            }   
            return w_len;  /* write() returns -1 on failure. */
        }   

        total += w_len; 
        pbuf = ((const char *)pbuf) + w_len; 
        len -= w_len; 
    }   

    return total;
}

/*******************************************************************************
* Function Name  : serial_read
* Description    : read data from serial file
* Input          : int fd // serial file handler
                   void *pbuf // data buf point
                   int count // total data len
* Output         : void *pbuf // data buf point
* Return         : int // read data len
*******************************************************************************/
int serial_read(int fd, void* pbuf, int len)
{
    return read(fd, pbuf, len);
}

