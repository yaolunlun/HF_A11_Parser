#ifndef __WIFI_PARSE__
#define __WIFI_PARSE__

#define WIFI_RET_SUCCESS            "+ok"
#define WIFI_RET_FAIL               "+ERR"
#define WIFI_RETDATA_SUCCESS        "+ok="
#define WIFI_RETDATA_FAIL           "+ERR="

#define WIFI_STRING_MAX_SIZE        100
#define WIFI_PARSE_LOOP_TIME        20      // ms

typedef enum {
    WIFI_CMD_AT = 0,
    WIFI_CMD_E,
    WIFI_CMD_ENTM     ,
    WIFI_CMD_NETP,
    WIFI_CMD_UART,
    WIFI_CMD_UARTF,
    WIFI_CMD_UARTFT,
    WIFI_CMD_UARTFL,
    WIFI_CMD_TMODE,
    WIFI_CMD_WMODE,
    WIFI_CMD_WSKEY,
    WIFI_CMD_WSSSID,
    WIFI_CMD_WSLK,
    WIFI_CMD_WEBU,
    WIFI_CMD_WAP,
    WIFI_CMD_WAKEY,
    WIFI_CMD_HIDESSID,
    WIFI_CMD_MSLP,
//    WIFI_CMD_WSCAN,
    WIFI_CMD_TCPLK,
    WIFI_CMD_TCPDIS,
    WIFI_CMD_WANN,
    WIFI_CMD_LANN,
    WIFI_CMD_DHCPDEN,
    WIFI_CMD_DHCPGW,
    WIFI_CMD_TCPTO,
    WIFI_CMD_MAXSK,
    WIFI_CMD_TCPB,
    WIFI_CMD_TCPPTB,
    WIFI_CMD_TCPADDB,
    WIFI_CMD_TCPTOP,
    WIFI_CMD_TCPLKB,
    WIFI_CMD_EPHY,
    WIFI_CMD_STTC,
    WIFI_CMD_DOMAIN,
    WIFI_CMD_FRLDEN,
    WIFI_CMD_RELD,
    WIFI_CMD_Z,
    WIFI_CMD_MID,
    WIFI_CMD_WRMID,
    WIFI_CMD_VER,
//    WIFI_CMD_H,
    WIFI_CMD_FVEW,
    WIFI_CMD_FVER,
    WIFI_CMD_WMAC,
    WIFI_CMD_PING,
    WIFI_CMD_FEPHY,

    WIFI_CMD_NUM,
} wifi_cmd_e;

typedef struct {
    char        cmd[WIFI_STRING_MAX_SIZE];      // the command string
    int         r_wait;                       // unit: ms, get wait milliseconds
    int         w_wait;                       // unit: ms, set wait milliseconds
} wifi_cmd_t;

int wifi_init(void);
int wifi_at_test(void);
int wifi_at_get(wifi_cmd_e cmd, char *ret);
int wifi_at_set(wifi_cmd_e cmd, const char *value, char *ret);

#endif

