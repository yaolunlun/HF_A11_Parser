TARGET = wifi_parser

C_FLAGS += -Wall -O3
LD_FLAGS += 

RELTARGET = release/$(TARGET)

SOURCES = $(wildcard *.c)
HEADERS = $(wildcard *.h)

RELOBJFILES = $(SOURCES:%.c=release/%.o)
RELLDFLAGS =

COMPILE = gcc $(C_FLAGS) -c
LINK = gcc $(LD_FLAGS)


all: release

release: $(RELTARGET)

$(RELTARGET): $(RELOBJFILES)
	$(LINK) -o $@ $^ $(RELLDFLAGS)


$(RELOBJFILES): release/%.o: %.c $(HEADERS)
	@mkdir -p release
	$(COMPILE) -o $@ $<

clean:
	-$(RM) -rf release *.d

.PHONY: clean release

