#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <termios.h>

int serial_open(const char* pdev, int baudrate, int parity, int databits, int stopbits, int vmin, int vtime);
int serial_write(int fd, const void *pbuf, int len);
int serial_read(int fd, void* pbuf, int len);
int serial_close(int fd);

#endif

